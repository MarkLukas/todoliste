package com.company;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;
import javax.swing.*;
import java.io.IOException;
//git token: YeLvi6F5-ttdD_2Tnauy
public class Main {
    private static String gewaehlteListe = ""; //Zuletzt ausgewählte Liste
    private static String[] todoListe = new String[0]; //Zuweisung von leerem Array nötig, damit createInterface() bei Aufrufen des Programms keinen Fehler meldet

    public static void main(String[] args) {
        createInterface();
    }

    public static void createInterface (){ //Erstellt grafisches Interface und weist Buttons Funktionen zu

        JFrame frame = new JFrame();
        frame.setTitle("To-Do-Liste");
        JPanel panel = new JPanel();
        frame.add(panel);
        JButton button1 = new JButton("Liste 1");
        button1.addActionListener(e -> chooseList("something.txt", frame));
        JButton button2 = new JButton("Liste 2");
        button2.addActionListener(e-> chooseList("somethingelse.txt", frame));
        JButton button3 = new JButton("Liste 3");
        button3.addActionListener(e-> chooseList("Aufgaben.txt", frame));
        panel.add(button1);
        panel.add(button2);
        panel.add(button3);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        //liest Elemente der gewählten Liste aus dem globalen Array aus und stellt erstellt Labels
        for(int i = 0; i < todoListe.length; i++){
            panel.add(new JLabel(todoListe[i]));
        }

        JButton addNewTask = new JButton("neue Aufgabe");
        addNewTask.addActionListener(e-> writeNewTask(frame));
        panel.add(addNewTask);
        JButton deleteTask = new JButton("Aufgaben Löschen");
        deleteTask.addActionListener(e-> deleteTask(frame));
        panel.add(deleteTask);

        frame.pack();
        frame.setVisible(true);

    }

    //wird bei Auswahl einer Liste aufgerufen. Es liest das übergebene Texdokument und überschreibt globales Array mit Inhalt des Textdokuments
    public static void chooseList(String document, JFrame frame){
        try {
            gewaehlteListe = document;
            File textfile = new File(document);
            Scanner reader = new Scanner(textfile);
            int howManyLines = 0;
            //zählt, wie viele Elemente im Textdokument vorhanden sind, damit Array von passender Länge erstellt werden kann
            while(reader.hasNextLine()){
                howManyLines++;
                reader.nextLine();
            }
            reader.close();

            //Erstellt zweiten Filereader, da der erste am ende des Dokuments angelangt ist.
            Scanner secondReader = new Scanner(textfile);
            String[] aufgabenliste = new String[howManyLines];
            for(int i = 0; i < aufgabenliste.length; i++){ //Füllt Array mit Elementen des Texdokuments
                aufgabenliste[i] = secondReader.nextLine();
            }
            todoListe =  aufgabenliste; //überschreibt globales Array mit aufgabenliste.

            frame.dispose();
            createInterface(); //schließt die Benutzeroberfläche und ruft sie neu auf, um Änderung sofort darzustellen.

        } catch (FileNotFoundException e) {
            System.out.println("An error occurred");
            e.printStackTrace();
        }
    }


    public static void writeNewTask(JFrame frame) { //erstellt neue Aufgabe in derzeit ausgewählter Liste
        if (!gewaehlteListe.equals("")) { //Wenn keine Liste ausgewählt ist, passiert nichts
            frame.dispose(); //schließe Benutzeroberfläche
            try {

                FileWriter writer = new FileWriter(gewaehlteListe, true);
                //Erstellt Benutzeroberfläche mit Eingabefeld
                JFrame inputFrame = new JFrame();
                inputFrame.setTitle("Erstelle Aufgabe");
                JPanel panel = new JPanel();
                inputFrame.add(panel);
                JTextField inputField = new JTextField("Aufgabe", 15);
                panel.add(inputField);
                panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

                JButton transmitInput = new JButton("Aufgabe erstellen");
                transmitInput.addActionListener(e -> { //schreibt die eingetragene Aufgabe in das Textdokument
                    try {
                        writer.write("\n" + inputField.getText());

                        writer.close();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                });
                transmitInput.addActionListener(e -> chooseList(gewaehlteListe, inputFrame));
                panel.add(transmitInput);
                inputFrame.pack();
                inputFrame.setVisible(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void deleteTask(JFrame frame) {
        if(!gewaehlteListe.equals("")) {
            frame.dispose();

            JFrame deletionFrame = new JFrame();
            deletionFrame.setTitle("Lösche Aufgabe");
            JPanel panel = new JPanel();
            deletionFrame.add(panel);
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

            //erstellt aus dem globalen Array eine Liste von JButtons
            //for-Schleife muss mit 1 beginnen, da Textdokumente mit einer leeren Zeile beginnen
            for(int i = 0; i < todoListe.length; i++){
                if(!todoListe[i].equals("")) {
                    System.out.println(i + todoListe[i]);
                    JButton button = new JButton(todoListe[i]);
                    int finalI = i; //nötig aus lambda gründen

                    //Wenn gedrückt, wird die korrespondierende Stelle im globalen Array mit "" ersetzt
                    button.addActionListener(e -> {
                        todoListe[finalI] = "";
                        button.setVisible(false);
                    });
                    panel.add(button);
                }
            }
            JButton commitDeletes = new JButton("Löschen");
            panel.add(commitDeletes);

            commitDeletes.addActionListener(e ->{
                //überträgt Punkte aus dem globalen Array in das gewählte Textdokument, wenn die Stelle NICHT "" ist
                try {
                    new FileWriter(gewaehlteListe).close();
                    FileWriter writer = new FileWriter(gewaehlteListe, true);
                    for(int i = 0; i < todoListe.length; i++){
                        if(!todoListe[i].equals("")){
                            writer.write("\n" + todoListe[i]);
                        }
                    }
                    writer.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                deletionFrame.dispose();
                createInterface();
            });

            deletionFrame.pack();
            deletionFrame.setVisible(true);
        }
    }
    //füge DeleteTask hinzu. Frage nach Listenpunkt ab, erstell Array aber lass die entsprechende Zahl aus, erstell neues Dokument aus Array.
}
